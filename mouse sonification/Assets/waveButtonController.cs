﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class waveButtonController : MonoBehaviour
{

    public AudioClip[] waveFiles;
    public GameObject[] targetAudioSourcesHC;


    public Transform cursor;
    GameObject currentModeActive;

    GameObject[] waveButtons, targetAudioSources;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void changeAudioWave(int waveFileIndex)
    {


        waveButtons = GameObject.FindGameObjectsWithTag("waveButton");
        targetAudioSources = GameObject.FindGameObjectsWithTag("targetAudioSource");

        foreach (GameObject aWB in waveButtons)
        {
            aWB.GetComponent<Button>().interactable = true;
        }

        /* foreach (GameObject aTAS in targetAudioSources)
         {
             if (aTAS.transform.parent.gameObject.activeSelf == true)
             {
                 currentModeActive = aTAS.transform.parent.gameObject;
             }

             aTAS.transform.parent.gameObject.SetActive(true);
             Debug.Log("changing clip");
             aTAS.GetComponent<AudioSource>().clip = waveFiles[waveFileIndex];
             Debug.Log("clip changed");
             //aTAS.GetComponent<AudioSource>().Play();
             aTAS.transform.parent.gameObject.SetActive(false);
         }*/

        foreach (GameObject aTAS in targetAudioSourcesHC)
        {
            if (aTAS.transform.parent.gameObject.activeSelf == true)
            {
                currentModeActive = aTAS.transform.parent.gameObject;
            }

            //aTAS.transform.parent.gameObject.SetActive(true);
            Debug.Log("changing clip");
            AudioSource[] audioSources = aTAS.GetComponents<AudioSource>();

            foreach (AudioSource aAS in audioSources)
            { 
                aAS.clip = waveFiles[waveFileIndex];
                aAS.Play();
            }


           // aTAS.transform.parent.gameObject.SetActive(false);
        }

        gameObject.GetComponent<Button>().interactable = false;
        currentModeActive.SetActive(true);

        cursor.localPosition = new Vector3(0f, 0f, -1f);
    }
}
