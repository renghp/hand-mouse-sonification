﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonController : MonoBehaviour
{

    public GameObject soniMode;
    public Transform cursor;

    GameObject[] soniModes, soniButtons;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void turnOffAllModes()
    {
        soniModes = GameObject.FindGameObjectsWithTag("sonificationMode");

        foreach (GameObject aSM in soniModes)
        {
            aSM.SetActive(false);
        }

        soniButtons = GameObject.FindGameObjectsWithTag("soniButton");

        foreach (GameObject aSB in soniButtons)
        {
            aSB.GetComponent<Button>().interactable = true;
        }

    }
    public void turnOnMode()
    {
        soniModes = GameObject.FindGameObjectsWithTag("sonificationMode");

        foreach (GameObject aSM in soniModes)
        {
            aSM.SetActive(false);
        }

        soniButtons = GameObject.FindGameObjectsWithTag("soniButton");

        foreach (GameObject aSB in soniButtons)
        {
            aSB.GetComponent<Button>().interactable = true;
        }

        soniMode.SetActive(true);
        gameObject.GetComponent<Button>().interactable = false;

        cursor.localPosition = new Vector3(0f, 0f, -1f);
    }

    public void quitApp()
    {
        Application.Quit();
    }
}
