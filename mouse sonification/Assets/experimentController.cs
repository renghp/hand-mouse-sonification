﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GoogleSheetsToUnity;
using GoogleSheetsToUnity.Utils;
using GreenerGames;
using System.Globalization;
public class experimentController : MonoBehaviour
{
    // Start is called before the first frame update

    public readonly string sheetId = "14Ud5EoNH_JSbjzJ29q9KT3NiRteqX5aMWEV7-PTGSfw"; // for BVI
    //public readonly string sheetId = "1yb6On7CxFWL6oFAj7T8QvZjbpfmSUOIgSTslupprSCw"; // for SIGHTED


    public readonly string worksheetName = "Sheet1";

    GameObject[] targetAudioSources;

    public AudioClip[] instructionAudioFiles;
    int stepCount, buttonCount, instructionCount;
    Vector3[] randomizedPos = new Vector3[10];
    public GameObject[] soniButtons;
    GameObject[] randomizedSoniButtons = new GameObject[7];
    int seedPos, seedMode;
    public Transform cursor;
    Vector2 lastWorldPosUsed;
    System.Random rnd1;

    bool askedForInstructionsAgain = false;

    String timeStamp;

    Vector3[] tutorialPos = new Vector3[3];
    private float timer = 0.0f;

    bool testIsOn = false;
    bool instructionIsOn = true;
    bool tutorialIsOn = false;

    public float userAdjustedVolume = 1f;

    void Start()
    {


        timeStamp = DateTime.Now.ToString();

        Debug.Log(timeStamp);

        rnd1 = new System.Random();

        seedPos = rnd1.Next(0, 9);
        seedMode = rnd1.Next(0, 7);


        tutorialPos[0] = new Vector3(-6f, 3, 0f);
        tutorialPos[1] = new Vector3(0f, 0, 0f);
        tutorialPos[2] = new Vector3(6f, -3, 0f);

        rafflePositions(seedPos);


        switch (seedMode)
        {
            case 0:
                randomizedSoniButtons[0] = soniButtons[2];
                randomizedSoniButtons[1] = soniButtons[1];
                randomizedSoniButtons[2] = soniButtons[0];
                randomizedSoniButtons[3] = soniButtons[6];
                randomizedSoniButtons[4] = soniButtons[4];
                randomizedSoniButtons[5] = soniButtons[3];
                randomizedSoniButtons[6] = soniButtons[5];
                break;
            case 1:
                randomizedSoniButtons[0] = soniButtons[5];
                randomizedSoniButtons[1] = soniButtons[4];
                randomizedSoniButtons[2] = soniButtons[3];
                randomizedSoniButtons[3] = soniButtons[2];
                randomizedSoniButtons[4] = soniButtons[0];
                randomizedSoniButtons[5] = soniButtons[6];
                randomizedSoniButtons[6] = soniButtons[1];
                break;
            case 2:
                randomizedSoniButtons[0] = soniButtons[1];
                randomizedSoniButtons[1] = soniButtons[0];
                randomizedSoniButtons[2] = soniButtons[6];
                randomizedSoniButtons[3] = soniButtons[5];
                randomizedSoniButtons[4] = soniButtons[3];
                randomizedSoniButtons[5] = soniButtons[2];
                randomizedSoniButtons[6] = soniButtons[4];
                break;
            case 3:
                randomizedSoniButtons[0] = soniButtons[3];
                randomizedSoniButtons[1] = soniButtons[2];
                randomizedSoniButtons[2] = soniButtons[1];
                randomizedSoniButtons[3] = soniButtons[0];
                randomizedSoniButtons[4] = soniButtons[5];
                randomizedSoniButtons[5] = soniButtons[4];
                randomizedSoniButtons[6] = soniButtons[6];
                break;
            case 4:
                randomizedSoniButtons[0] = soniButtons[6];
                randomizedSoniButtons[1] = soniButtons[5];
                randomizedSoniButtons[2] = soniButtons[4];
                randomizedSoniButtons[3] = soniButtons[3];
                randomizedSoniButtons[4] = soniButtons[1];
                randomizedSoniButtons[5] = soniButtons[0];
                randomizedSoniButtons[6] = soniButtons[2];
                break;
            case 5:
                randomizedSoniButtons[0] = soniButtons[4];
                randomizedSoniButtons[1] = soniButtons[3];
                randomizedSoniButtons[2] = soniButtons[2];
                randomizedSoniButtons[3] = soniButtons[1];
                randomizedSoniButtons[4] = soniButtons[6];
                randomizedSoniButtons[5] = soniButtons[5];
                randomizedSoniButtons[6] = soniButtons[0];
                break;
            case 6:
                randomizedSoniButtons[0] = soniButtons[0];
                randomizedSoniButtons[1] = soniButtons[6];
                randomizedSoniButtons[2] = soniButtons[5];
                randomizedSoniButtons[3] = soniButtons[4];
                randomizedSoniButtons[4] = soniButtons[2];
                randomizedSoniButtons[5] = soniButtons[1];
                randomizedSoniButtons[6] = soniButtons[3];
                break;
        }

        timer = 0;
        randomizedSoniButtons[0].GetComponent<buttonController>().turnOnMode();
        changeTargetLocation(tutorialPos[2]);

        Debug.Log("current mode seed is: " + seedMode);

        Debug.Log(randomizedSoniButtons[0].name + " | " +
        randomizedSoniButtons[1].name + " | " +
        randomizedSoniButtons[2].name + " | " +
        randomizedSoniButtons[3].name + " | " +
        randomizedSoniButtons[4].name + " | " +
        randomizedSoniButtons[5].name + " | " +
        randomizedSoniButtons[6].name);

        stepCount = -3;
        buttonCount = 0;
        instructionCount = 0;


        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[1];
        gameObject.GetComponent<AudioSource>().Play();


    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            Debug.Log("volume up");

            userAdjustedVolume += 0.05f;

            if (userAdjustedVolume > 1f)
                userAdjustedVolume = 1f;

            changeTargetVolume(userAdjustedVolume);
            gameObject.GetComponent<AudioSource>().volume = userAdjustedVolume;

        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            Debug.Log("volume down");
            userAdjustedVolume -= 0.05f;

            if (userAdjustedVolume < 0.05f)
                userAdjustedVolume = 0.05f;

            changeTargetVolume(userAdjustedVolume);
            gameObject.GetComponent<AudioSource>().volume = userAdjustedVolume;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (instructionIsOn)
        {
            randomizedSoniButtons[0].GetComponent<buttonController>().turnOffAllModes();

            switch (instructionCount)
            {
                case 0:
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[0];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[2];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                        instructionCount++;
                    }
                    break;
                case 1:
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[2];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[1];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;
                case 2:
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[0];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[3];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                        instructionCount++;
                    }
                    break;
                case 3:
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[3];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[2];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;
                case 4:
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[0];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[4];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                        instructionCount++;
                    }
                    break;
                case 5:
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[4];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[3];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;
                case 6:
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[0];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        instructionIsOn = false;
                        tutorialIsOn = true;
                        testIsOn = false;
                      //  gameObject.GetComponent<AudioSource>().Stop();
                      //  randomizedSoniButtons[0].GetComponent<buttonController>().turnOnMode();

                        instructionCount++;
                        instructionCount++;
                    }
                    break;
                case 7:
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
                    {
                        // gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[3];
                        // gameObject.GetComponent<AudioSource>().Play();
                        

                        instructionCount++;

                        instructionIsOn = false;
                        tutorialIsOn = true;
                        testIsOn = false;
                       // gameObject.GetComponent<AudioSource>().Stop();
                       // randomizedSoniButtons[0].GetComponent<buttonController>().turnOnMode();

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[4];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;

            }


        }

        if (tutorialIsOn)
        {
            randomizedSoniButtons[0].GetComponent<buttonController>().turnOffAllModes();

            switch (instructionCount)
            {
                case 8:                                                         //instruction for soni method
                    if (randomizedSoniButtons[buttonCount].name == "pitchOnly")
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[5];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                    }
                    else if (randomizedSoniButtons[buttonCount].name == "spatialPitch")
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[6];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                    }
                    else if (randomizedSoniButtons[buttonCount].name == "channelBasedPitch")
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[7];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                    }
                    else if (randomizedSoniButtons[buttonCount].name == "alternatedPitch")
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[8];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                    }
                    else if (randomizedSoniButtons[buttonCount].name == "spatialAlternatedPitch")
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[9];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                    }
                    else if (randomizedSoniButtons[buttonCount].name == "spatialHarmonicsOneD")
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[10];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                    }
                    else if (randomizedSoniButtons[buttonCount].name == "spatialHarmonicsTwoD")
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[11];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;
                    }
                    break;
                case 9:
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //listen again?
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[0];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        instructionCount = 13;

                        instructionIsOn = false;
                        tutorialIsOn = false;
                        testIsOn = true;
                        gameObject.GetComponent<AudioSource>().Stop();
                        timer = 0;
                        randomizedSoniButtons[buttonCount].GetComponent<buttonController>().turnOnMode();
                    }
                    break;
                case 10:                                                         //mid 1
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[12];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                    {
                        //gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[1];
                        gameObject.GetComponent<AudioSource>().Pause();
                        instructionCount = 8;
                    }
                    break;
                case 11:
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //listen again?
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[0];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    break;
                case 12:                                                         //start tutorial
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
                    {
                        //gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[12];
                        //gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                        instructionIsOn = false;
                        tutorialIsOn = false;
                        testIsOn = true;
                        gameObject.GetComponent<AudioSource>().Stop();
                        timer = 0;
                        randomizedSoniButtons[buttonCount].GetComponent<buttonController>().turnOnMode();

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[12];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount --;
                    }
                    break;
                case 13:
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //listen again?
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[0];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        instructionCount = 8;

                        instructionIsOn = false;
                        tutorialIsOn = false;
                        testIsOn = true;
                        gameObject.GetComponent<AudioSource>().Stop();
                        timer = 0;
                        randomizedSoniButtons[buttonCount].GetComponent<buttonController>().turnOnMode();
                    }
                    break;
                case 14:                                                         //start experiment
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
                    {
                        //gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[12];
                        //gameObject.GetComponent<AudioSource>().Play();
                       // instructionCount = 8;

                        instructionIsOn = false;
                        tutorialIsOn = false;
                        testIsOn = true;
                        gameObject.GetComponent<AudioSource>().Stop();
                        timer = 0;
                        randomizedSoniButtons[buttonCount].GetComponent<buttonController>().turnOnMode();

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[13];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;
                case 15:                                                    //tlx1
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //press number or listen again?
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[20];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();
                        
                    }
                    else if(Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(1, "tlx1", "mental", numberPressed);

                    }
                    break;
                case 16:

                    if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(1, "tlx1", "mental", numberPressed);

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))  //user decided to listen again
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[14];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;

                case 17:                                                    //tlx2
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //press number or listen again?
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[20];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                    }
                    else if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(2, "tlx2", "physical", numberPressed);

                    }
                    break;
                case 18:

                    if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(2, "tlx2", "physical", numberPressed);

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))  //user decided to listen again
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[15];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;

                case 19:                                                    //tlx3
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //press number or listen again?
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[20];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                    }
                    else if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(3, "tlx3", "temporal", numberPressed);

                    }
                    break;
                case 20:

                    if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(3, "tlx3", "temporal", numberPressed);

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))  //user decided to listen again
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[16];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;

                case 21:                                                    //tlx4
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //press number or listen again?
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[20];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                    }
                    else if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(4, "tlx4", "performance", numberPressed);

                    }
                    break;
                case 22:
                    if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(4, "tlx4", "performance", numberPressed);

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))  //user decided to listen again
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[17];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;

                case 23:                                                    //tlx5
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //press number or listen again?
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[20];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                    }
                    else if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(5, "tlx5", "effort", numberPressed);

                    }
                    break;
                case 24:
                    if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(5, "tlx5", "effort", numberPressed);

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))  //user decided to listen again
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[18];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;
                case 25:                                                    //tlx6
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //press number or listen again?
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[20];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    else if (Input.GetKeyDown("u"))     //hard skip
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                    }
                    else if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        instructionCount++;
                        gameObject.GetComponent<AudioSource>().Stop();

                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(6, "tlx6", "frustration", numberPressed);

                    }
                    break;
                case 26:
                    if (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[5]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Alpha7))
                    {
                        string numberPressed = getNumberKeyFromUser();

                        uploadTLXresult(6, "tlx6", "frustration", numberPressed);

                    }
                    else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))  //user decided to listen again
                    {
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[19];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount--;
                    }
                    break;
                case 27:
                    gameObject.GetComponent<AudioSource>().Stop();
                    buttonCount++;
                    timer = 0;
                    stepCount = -3;
                    Debug.Log("This mode is over");

                    if (buttonCount <= 2)                   //condition to not end the test ('else' ends it). original one for the 7 methods was if (buttonCount <= 6)       
                    {
                        instructionCount = 8;
                        instructionIsOn = false;
                        tutorialIsOn = true;
                        testIsOn = false;
                        randomizedSoniButtons[buttonCount].GetComponent<buttonController>().turnOffAllModes();

                    }
                    else
                    {
                        Debug.Log("buttonCount = " + buttonCount);
                        randomizedSoniButtons[buttonCount - 1].GetComponent<buttonController>().turnOffAllModes();

                        testIsOn = false;
                        Debug.Log("Test is over");

                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[21];
                        gameObject.GetComponent<AudioSource>().Play();
                        instructionCount++;

                    }
                    seedPos = rnd1.Next(0, 9);
                    rafflePositions(seedPos);

                    break;
                case 28:
                    if (!gameObject.GetComponent<AudioSource>().isPlaying)      //close when the message is done
                    {
                        Application.Quit();
                    }
                    break;
            }
        }

       

        else if (testIsOn)
        {
            timer += Time.deltaTime;

            if (askedForInstructionsAgain && !gameObject.GetComponent<AudioSource>().isPlaying)
            {
                askedForInstructionsAgain = false;
                changeTargetVolume(userAdjustedVolume);
            }

            if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) && stepCount <= 0)     // user wants to listen to method instruction again during the tutorial
            {
                askedForInstructionsAgain = true;

                changeTargetVolume(0.05f);

                if (randomizedSoniButtons[buttonCount].name == "pitchOnly")
                {
                    gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[5];
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (randomizedSoniButtons[buttonCount].name == "spatialPitch")
                {
                    gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[6];
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (randomizedSoniButtons[buttonCount].name == "channelBasedPitch")
                {
                    gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[7];
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (randomizedSoniButtons[buttonCount].name == "alternatedPitch")
                {
                    gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[8];
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (randomizedSoniButtons[buttonCount].name == "spatialAlternatedPitch")
                {
                    gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[9];
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (randomizedSoniButtons[buttonCount].name == "spatialHarmonicsOneD")
                {
                    gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[10];
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (randomizedSoniButtons[buttonCount].name == "spatialHarmonicsTwoD")
                {
                    gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[11];
                    gameObject.GetComponent<AudioSource>().Play();
                }
            }

            else if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
            {                                   // in order to accept mouse left click

                Debug.Log("time taken for last trial: " + timer);

                Vector2 currentCursorWorldPos = new Vector2(cursor.position.x, cursor.position.y);
                float distanceCursorToTarget = Vector2.Distance(currentCursorWorldPos, lastWorldPosUsed);
                float distanceX = Mathf.Abs(cursor.position.x - lastWorldPosUsed.x);
                float distanceY = Mathf.Abs(cursor.position.y - lastWorldPosUsed.y);
                Debug.Log("total distance: " + distanceCursorToTarget + " X distance: " + distanceX + " Y distance: " + distanceY);



                if (stepCount >= 0)
                { 

                    Debug.Log("buttonCount = " + buttonCount);

                    List<string> list = new List<string>() {
                    timeStamp,
                    timer.ToString(CultureInfo.InvariantCulture),
                    distanceCursorToTarget.ToString(CultureInfo.InvariantCulture),
                    distanceX.ToString(CultureInfo.InvariantCulture),
                    distanceY.ToString(CultureInfo.InvariantCulture),
                    stepCount.ToString(),
                    buttonCount.ToString(),
                    randomizedSoniButtons[buttonCount].name,
                    seedPos.ToString(),
                    seedMode.ToString()
                    };

                    SpreadsheetManager.Append(new GSTU_Search(sheetId,
                   randomizedSoniButtons[buttonCount].name), new ValueRange(list), null);

                    Debug.Log("escreveu?");

                }

                switch (stepCount)
                {
                    case -3:                                    //tutorial run
                        changeTargetLocation(tutorialPos[0]);
                        timer = 0;

                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case -2:                                    //tutorial run
                        changeTargetLocation(tutorialPos[1]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case -1:                                    //tutorial run
                        changeTargetLocation(tutorialPos[2]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 0:
                        changeTargetLocation(randomizedPos[0]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        instructionIsOn = false;
                        tutorialIsOn = true;
                        testIsOn = false;
                        randomizedSoniButtons[buttonCount].GetComponent<buttonController>().turnOffAllModes();
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[13];
                        gameObject.GetComponent<AudioSource>().Play();
                        
                                                
                        break;
                    case 1:
                        changeTargetLocation(randomizedPos[1]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 2:
                        changeTargetLocation(randomizedPos[2]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 3:
                        changeTargetLocation(randomizedPos[3]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 4:
                        changeTargetLocation(randomizedPos[4]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 5:
                        changeTargetLocation(randomizedPos[5]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 6:
                        changeTargetLocation(randomizedPos[6]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 7:
                        changeTargetLocation(randomizedPos[7]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 8:
                        changeTargetLocation(randomizedPos[8]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 9:
                        changeTargetLocation(randomizedPos[9]);
                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        break;
                    case 10:    //send to tlx1

                        timer = 0;
                        cursor.localPosition = new Vector3(0f, 0f, 0f);
                        stepCount++;
                        instructionCount = 15;

                        instructionIsOn = false;
                        tutorialIsOn = true;
                        testIsOn = false;
                        randomizedSoniButtons[buttonCount].GetComponent<buttonController>().turnOffAllModes();
                        gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[14];
                        gameObject.GetComponent<AudioSource>().Play();

                        break;

                    
                }







            }

        }

    }


    void uploadTLXresult(int tlxNumber, string tlxCode, string tlxType, string numberPressed)
    {

        List<string> list = new List<string>() {
                            timeStamp,
                            tlxCode,
                            numberPressed,
                            tlxType,
                            "",
                            stepCount.ToString(),
                            buttonCount.ToString(),
                            randomizedSoniButtons[buttonCount].name,
                            seedPos.ToString(),
                            seedMode.ToString()
                            };


            SpreadsheetManager.Append(new GSTU_Search(sheetId,
            "TLX" + randomizedSoniButtons[buttonCount].name), new ValueRange(list), null);

            Debug.Log("escreveu?");

            
            gameObject.GetComponent<AudioSource>().clip = instructionAudioFiles[tlxNumber+14];
            gameObject.GetComponent<AudioSource>().Play();
            instructionCount++;
        
    }
    void rafflePositions(int seed)
    {
        switch (seed)
        {
            case 0:
                randomizedPos[7] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[3] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[6] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[4] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[8] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[0] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[5] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[1] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[2] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[9] = new Vector3(-8.03f, -0.79f, 0f);
                break;
            case 1:
                randomizedPos[1] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[7] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[0] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[8] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[2] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[4] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[9] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[5] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[6] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[3] = new Vector3(-8.03f, -0.79f, 0f);
                break;
            case 2:
                randomizedPos[5] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[1] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[4] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[2] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[6] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[8] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[3] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[9] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[0] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[7] = new Vector3(-8.03f, -0.79f, 0f);
                break;
            case 3:
                randomizedPos[3] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[9] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[2] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[0] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[4] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[6] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[1] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[7] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[8] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[5] = new Vector3(-8.03f, -0.79f, 0f);
                break;
            case 4:
                randomizedPos[0] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[6] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[9] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[7] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[1] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[3] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[8] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[4] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[5] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[2] = new Vector3(-8.03f, -0.79f, 0f);
                break;
            case 5:
                randomizedPos[4] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[0] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[3] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[1] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[5] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[7] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[2] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[8] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[9] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[6] = new Vector3(-8.03f, -0.79f, 0f);
                break;
            case 6:
                randomizedPos[6] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[2] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[5] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[3] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[7] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[9] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[4] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[0] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[1] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[8] = new Vector3(-8.03f, -0.79f, 0f);
                break;
            case 7:
                randomizedPos[2] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[8] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[1] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[9] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[3] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[5] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[0] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[6] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[7] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[4] = new Vector3(-8.03f, -0.79f, 0f);
                break;
            case 8:
                randomizedPos[9] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[5] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[8] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[6] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[0] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[2] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[7] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[3] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[4] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[1] = new Vector3(-8.03f, -0.79f, 0f);
                break;
            case 9:
                randomizedPos[8] = new Vector3(-8.65f, 0.01f, 0f);
                randomizedPos[4] = new Vector3(4.97f, 3.04f, 0f);
                randomizedPos[7] = new Vector3(6.18f, 0.14f, 0f);
                randomizedPos[5] = new Vector3(-0.50f, 3.83f, 0f);
                randomizedPos[9] = new Vector3(-5.17f, 2.41f, 0f);
                randomizedPos[1] = new Vector3(6.77f, -4.80f, 0f);
                randomizedPos[6] = new Vector3(-6.03f, -0.33f, 0f);
                randomizedPos[2] = new Vector3(-8.97f, 4.65f, 0f);
                randomizedPos[3] = new Vector3(0.13f, -1.02f, 0f);
                randomizedPos[0] = new Vector3(-8.03f, -0.79f, 0f);
                break;
        }

        Debug.Log("current position seed is: " + seed);

        Debug.Log(randomizedPos[0] + " " +
        randomizedPos[1] + " " +
        randomizedPos[2] + " " +
        randomizedPos[3] + " " +
        randomizedPos[4] + " " +
        randomizedPos[5] + " " +
        randomizedPos[6] + " " +
        randomizedPos[7] + " " +
        randomizedPos[8] + " " +
        randomizedPos[9]);
    }

    void changeTargetLocation(Vector3 pos)
    {

        targetAudioSources = GameObject.FindGameObjectsWithTag("targetAudioSource");

        foreach (GameObject aTAS in targetAudioSources)
        {
            aTAS.transform.localPosition = pos;

            lastWorldPosUsed = new Vector2(aTAS.transform.position.x, aTAS.transform.position.y);
        }
    }

    void changeTargetVolume(float newVolume)
    {
        targetAudioSources = GameObject.FindGameObjectsWithTag("targetAudioSource");

        foreach (GameObject aTAS in targetAudioSources)
        {
            AudioSource[] multipleAS = aTAS.GetComponents<AudioSource>();

            foreach (AudioSource mAS in multipleAS)
            {
                mAS.volume = newVolume;
            }

        }
    }


    string getNumberKeyFromUser()
    {

        if (Input.GetKeyDown("[1]") || Input.GetKeyDown(KeyCode.Alpha1))
            return "1";
        else if (Input.GetKeyDown("[2]") || Input.GetKeyDown(KeyCode.Alpha2))
            return "2";
        else if (Input.GetKeyDown("[3]") || Input.GetKeyDown(KeyCode.Alpha3))
            return "3";
        else if (Input.GetKeyDown("[4]") || Input.GetKeyDown(KeyCode.Alpha4))
            return "4";
        else if (Input.GetKeyDown("[5]") || Input.GetKeyDown(KeyCode.Alpha5))
            return "5";
        else if (Input.GetKeyDown("[6]") || Input.GetKeyDown(KeyCode.Alpha6))
            return "6";
        else if (Input.GetKeyDown("[7]") || Input.GetKeyDown(KeyCode.Alpha7))
            return "7";
        else
            return " ";

    }
}


    

/*
 10-positions latin square:
7	3	6	4	8	0	5	1	2	9
1	7	0	8	2	4	9	5	6	3
5	1	4	2	6	8	3	9	0	7
3	9	2	0	4	6	1	7	8	5
0	6	9	7	1	3	8	4	5	2
4	0	3	1	5	7	2	8	9	6
6	2	5	3	7	9	4	0	1	8
2	8	1	9	3	5	0	6	7	4
9	5	8	6	0	2	7	3	4	1
8	4	7	5	9	1	6	2	3	0

*/

/*
7-modes latin square:
2	1	0	6	4	3	5
5	4	3	2	0	6	1
1	0	6	5	3	2	4
3	2	1	0	5	4	6
6	5	4	3	1	0	2
4	3	2	1	6	5	0
0	6	5	4	2	1	3
*/