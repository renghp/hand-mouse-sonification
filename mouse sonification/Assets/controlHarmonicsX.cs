﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class controlHarmonicsX : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform mousePos, targetPos;
    public AudioSource a0;
    public Text xFreq;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        float distX = Math.Abs(mousePos.position.x - targetPos.position.x) / 2f;

         if (distX >= -0.2f && distX <= 0.2f)
             a0.pitch = 0f;
         else if (distX >= 0.3f && distX <= 0.7f)
             a0.pitch = 0.5f;
         else if (distX >= 0.8f && distX <= 1.2f)
             a0.pitch = 1f;
         else if (distX >= 1.8f && distX <= 2.2f)
             a0.pitch = 2f;
         else if (distX >= 2.8f && distX <= 3.2f)
             a0.pitch = 3f;
         else if (distX >= 3.8f && distX <= 4.2f)
             a0.pitch = 4f;
         else if (distX >= 4.8f && distX <= 5.2f)
             a0.pitch = 5f;
         else
             a0.pitch = distX;
        

       /* if (distX >= -0.2f && distX <= 0.2f)
            a0.pitch = 1f;
        else if (distX >= 0.3f && distX <= 0.7f)
            a0.pitch = 1.5f;
        else if (distX >= 0.8f && distX <= 1.2f)
            a0.pitch = 2f;
        else if (distX >= 1.8f && distX <= 2.2f)
            a0.pitch = 3f;
        else if (distX >= 2.8f && distX <= 3.2f)
            a0.pitch = 4f;
        else if (distX >= 3.8f && distX <= 4.2f)
            a0.pitch = 5f;
        else if (distX >= 4.8f && distX <= 5.2f)
            a0.pitch = 6f;
        else
            a0.pitch = distX; */

        xFreq.text = (a0.pitch*100).ToString() + " Hz";
    }
}
