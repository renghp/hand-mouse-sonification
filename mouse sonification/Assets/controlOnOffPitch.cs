﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class controlOnOffPitch : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform mousePos, targetPos;
    public AudioSource a0;
    private float timer = 0.0f;

    public GameObject experimentController1, experimentController2;
    float volumeLimiter = 1f;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (experimentController1.activeSelf)
        {
            volumeLimiter = experimentController1.GetComponent<experimentController>().userAdjustedVolume;
        }
        else if (experimentController2.activeSelf)
        {
            volumeLimiter = experimentController2.GetComponent<experimentController>().userAdjustedVolume;
        }

        float distX = Math.Abs((mousePos.position.x - targetPos.position.x)/10);
        float distY = Math.Abs(mousePos.position.y - targetPos.position.y);


        //Vector2 mouse2D = new Vector2(mousePos.position.x, mousePos.position.y);
        //Vector2 target2D = new Vector2(targetPos.position.x, targetPos.position.y);

        //float dist2D = Vector2.Distance(mouse2D, target2D);

        // Debug.Log("dist = " + distX + ", " + distY);
        a0.pitch = distY + 0.4f;


        timer += Time.deltaTime;


        if (distX <= 0.04)
            a0.volume = volumeLimiter;

        else if (a0.volume == 0 && timer > 0.1f)
        {
            timer = 0;
            a0.volume = volumeLimiter;
            
        }

        else if (timer > distX/6f + 0.04f)
        {
            timer = 0;


            if (a0.volume == volumeLimiter)
                a0.volume = 0f;
            else
                a0.volume = volumeLimiter;

           // Debug.Log(distX);

        }
    }
}
