﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlSpatialPitch : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform mousePos, targetPos;
    public AudioSource a0;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        float distY = mousePos.position.y - targetPos.position.y;

        a0.pitch = distY;
    }
}
