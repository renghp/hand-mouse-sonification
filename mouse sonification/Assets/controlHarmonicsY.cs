﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class controlHarmonicsY : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform mousePos, targetPos;
    public AudioSource a0;
    public Text yFreq;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        float distY = Math.Abs(mousePos.position.y - targetPos.position.y)/2f;

           if (distY >= -0.2f && distY <= 0.2f)
               a0.pitch = 0f;
           else if (distY >= 0.3f && distY <= 0.7f)
               a0.pitch = 0.5f;
           else if(distY >= 0.8f && distY <= 1.2f)
               a0.pitch = 1f;
           else if (distY >= 1.8f && distY <= 2.2f)
               a0.pitch = 2f;
           else if (distY >= 2.8f && distY <= 3.2f)
               a0.pitch = 3f;
           else if (distY >= 3.8f && distY <= 4.2f)
               a0.pitch = 4f;
           else if (distY >= 4.8f && distY <= 5.2f)
               a0.pitch = 5f;
           else
               a0.pitch = distY;
        

      /*  if (distY >= -0.2f && distY <= 0.2f)
            a0.pitch = 1f;
        else if (distY >= 0.3f && distY <= 0.7f)
            a0.pitch = 1.5f;
        else if (distY >= 0.8f && distY <= 1.2f)
            a0.pitch = 2f;
        else if (distY >= 1.8f && distY <= 2.2f)
            a0.pitch = 3f;
        else if (distY >= 2.8f && distY <= 3.2f)
            a0.pitch = 4f;
        else if (distY >= 3.8f && distY <= 4.2f)
            a0.pitch = 5f;
        else if (distY >= 4.8f && distY <= 5.2f)
            a0.pitch = 6f;
        else
            a0.pitch = distY;*/


        yFreq.text = (a0.pitch * 100).ToString() + " Hz";
    }
}
