﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class controlBinauralPitch : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform mousePos, targetPos;
    public AudioSource aLeft, aRight;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //float pitch = 0;

        float distX = Math.Abs(mousePos.position.x - targetPos.position.x);
        float distY = Math.Abs(mousePos.position.y - targetPos.position.y);


        //Vector2 mouse2D = new Vector2(mousePos.position.x, mousePos.position.y);
        //Vector2 target2D = new Vector2(targetPos.position.x, targetPos.position.y);

        //float dist2D = Vector2.Distance(mouse2D, target2D);

        // Debug.Log("dist = " + distX + ", " + distY);
        aLeft.pitch = distX + 0.4f;
        aRight.pitch = distY + 0.4f;

       // Debug.Log(aLeft.pitch);
    }
}
