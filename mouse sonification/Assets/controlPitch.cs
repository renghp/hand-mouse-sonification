﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlPitch : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform mousePos, targetPos;
    public AudioSource a0;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //float distX = mousePos.position.x - targetPos.position.x;
        //float distY = mousePos.position.y - targetPos.position.y;


        Vector2 mouse2D = new Vector2(mousePos.position.x, mousePos.position.y);
        Vector2 target2D = new Vector2(targetPos.position.x, targetPos.position.y);

        float dist2D = Vector2.Distance(mouse2D, target2D);

       // Debug.Log("dist = " + distX + ", " + distY);
        a0.pitch = dist2D + 0.4f;
    }
}
